declare interface BusinessList {
  [key: string]: string
}

declare type sortBy = 'best_match' | 'rating' | 'review_count'
