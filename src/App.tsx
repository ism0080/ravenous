import React, { useState } from 'react'
import { render } from 'react-dom'

import './App.css'
import { RestaurantList, SearchBar } from 'components'
import Yelp from 'util/yelp'

const App = () => {
  const [businesses, setBusinesses] = useState<BusinessList[]>()

  const searchYelp = (term: string, location: string, sortBy: sortBy) => {
    Yelp.search(term, location, sortBy).then((business) => {
      setBusinesses(business)
    })
  }

  return (
    <div className='App'>
      <h1>ravenous</h1>
      <SearchBar searchYelp={searchYelp} />
      <RestaurantList businesses={businesses} />
    </div>
  )
}

render(<App />, document.getElementById('root'))
