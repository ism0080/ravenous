import axios, { AxiosInstance } from 'axios'

const apiKey = process.env.CLIENT_ID
const instance: AxiosInstance = axios.create({
  baseURL: 'https://cors-anywhere.herokuapp.com/https://api.yelp.com/v3/businesses/', // Prepended with CORS
  headers: { Authorization: `Bearer ${apiKey}` },
})

const yelp = {
  async search(term: string, location: string, sortBy: sortBy) {
    const res = await instance.get(`search?term=${term}&location=${location}&sort_by=${sortBy}`)
    const { businesses } = res.data
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    return businesses.map((business: any) => ({
      id: business.id,
      imgSrc: business.image_url,
      name: business.name,
      address: business.location.address1,
      city: business.location.city,
      state: business.location.state,
      zipCode: business.location.zip_code,
      category: business.categories[0].title,
      rating: business.rating,
      reviewCount: business.review_count,
      url: business.url,
    }))
  },
}

export default yelp
