import React from 'react'
import './restaurant-list.css'
import { Restaurant } from 'components'

export const RestaurantList = ({ businesses }: RestaurantListProps) => (
  <>
    {businesses && (
      <div className='BusinessList'>
        {businesses.map((store) => {
          return <Restaurant key={store.id} business={store} />
        })}
      </div>
    )}
  </>
)
