interface SearchYelpProps {
  searchYelp: (term: string, location: string, sortBy: sortBy) => void
}

interface SortOptions {
  [key: string]: sortBy
}
