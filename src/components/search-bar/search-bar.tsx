import React, { useState } from 'react'

import './search-bar.css'

export const SearchBar = ({ searchYelp }: SearchYelpProps) => {
  const [term, setTerm] = useState<string>('')
  const [location, setLocation] = useState<string>('')
  const [sortBy, setSortBy] = useState<sortBy>('best_match')
  const sortByOptions: SortOptions = {
    'Best Match': 'best_match',
    'Highest Rated': 'rating',
    'Most Reviewed': 'review_count',
  }

  const getSortByClass = (sortByOption: sortBy) => {
    if (sortBy === sortByOption) {
      return 'active'
    }
    return ''
  }

  const handleClickSortByChange = (sortOption: sortBy) => {
    setSortBy(sortOption)
    if (term !== '' && location !== '' && !sortBy) {
      searchYelp(term, location, sortBy)
    }
  }

  const handleKeySortByChange = (event: React.KeyboardEvent) => {
    if (event.key === 'Enter') {
      searchYelp(term, location, sortBy)
    }
  }

  const handleTermChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setTerm(event.target.value)
  }

  const handleLocationChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setLocation(event.target.value)
  }

  const handleSearch = (event: React.MouseEvent<HTMLDivElement>) => {
    searchYelp(term, location, sortBy)
    event.preventDefault()
  }

  const renderSortByOptions = () => {
    return Object.keys(sortByOptions).map((sortByOption, index) => {
      const sortByOptionValue = sortByOptions[sortByOption]
      return (
        <li
          role='list'
          key={index}
          className={getSortByClass(sortByOptionValue)}
          onClick={() => handleClickSortByChange(sortByOptionValue)}
        >
          {sortByOption}
        </li>
      )
    })
  }

  return (
    <div className='SearchBar'>
      <div className='SearchBar-sort-options'>
        <ul>{renderSortByOptions()}</ul>
      </div>
      <div className='SearchBar-fields'>
        <input placeholder='Search Businesses' onChange={handleTermChange} onKeyDown={handleKeySortByChange} />
        <input placeholder='Where?' onChange={handleLocationChange} onKeyDown={handleKeySortByChange} />
      </div>
      <div className='SearchBar-submit' onClick={handleSearch}>
        <span>Let&apos;s Go</span>
      </div>
    </div>
  )
}
